## Usage:
Either **clone** this repo or use `wget` or `curl` to **download** the Luatch module:
* `git clone https://gitlab.com/CodeWeaver/Luatch.git`
* `wget https://gitlab.com/CodeWeaver/Luatch/raw/master/luatch.lua`
* `curl -O https://gitlab.com/CodeWeaver/Luatch/raw/master/luatch.lua`

Use `require "luatch"` to get a benchmarker `function(f, ...)`, which accepts
another function `f(...)` and its arguments; returns the execution time.

Example use:

```lua
timer = require "luatch"

function split(str, sep)
	--[[ Split a string (with given delimiter sep) into an array ]]
	if sep == nil then sep = "%s" end
	local values = {}
	for s in string.gmatch(str, "([^"..sep.."]+)") do values[#values+1] = s end
	return values
end

print(timer(split, "comma,delimited,list", ','))
```